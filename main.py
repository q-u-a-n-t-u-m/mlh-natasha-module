#!/usr/bin/python
# -*- coding: utf-8 -*-

import pymorphy2
import requests
import re	

sin_not_mask =  {"NOUN":0, # для тех, которые 1, синонимы искаться не будут
			 "ADJF":0,
			 "ADJS":0,
			 "COMP":0,
			 "VERB":0,
			 "INFN":0,
			 "PRTF":0,
			 "PRTS":0,
			 "GRND":0,
			 "NUMR":1,
			 "ADVB":0,
			 "NPRO":1,
			 "PRED":1,
			 "PREP":1,
			 "CONJ":1,
			 "PRCL":1,
			 "INTJ":1}

def pos_parse(pos): # часть речи из тега в русский вариант
	_dict = {"NOUN":u"существительное",
			 "ADJF":u"полное прилагательное",
			 "ADJS":u"краткое прилагательное",
			 "COMP":u"компаратив",
			 "VERB":u"глагол",
			 "INFN":u"глагол-инфинитив",
			 "PRTF":u"полное причастие",
			 "PRTS":u"краткое причастие",
			 "GRND":u"деепричастие",
			 "NUMR":u"числительное",
			 "ADVB":u"наречие",
			 "NPRO":u"местоимение-существительное",
			 "PRED":u"предикатив",
			 "PREP":u"предлог",
			 "CONJ":u"союз",
			 "PRCL":u"частица",
			 "INTJ":u"междометие"}
	return _dict[pos]

def case_parse(_case): # падеж
	_dict = {"nomn":u"именительный",
			 "gent":u"родительный",
			 "datv":u"дательный",
			 "accs":u"винительный",
			 "ablt":u"творительный",
			 "loct":u"предложный",
			 "voct":u"звательный",
			 "gen2":u"второй родительный (частичный)",
			 "acc2":u"второй винительный",
			 "loc2":u"второй предложный (местный)",
			 "None":u"без падежа"}
	return _dict[_case]

def number_parse(num): # число
	_dict = {"sing":u"единственное",
			 "plur":u"мноэественное",
			 "None":u"без числа"}
	return _dict[num]

# сама строка
my_in = u"Привет! Как дела? Это тестовая строка, каждое слово проверится на часть речи и будет сделана попытка найти нормальную форму. Кракозябры пуляются хренопупырышками, а пуморф умеет работать не по словарю..."

# бьем на слова
result = re.findall( u"[а-яА-Яa-zA-Z]+", my_in+u" " )

print u"Строка: "+my_in
print u"Количество слов: "+str(len(result))
print u"Слова:"

print "---------------------------------"

# объекты для морфоанализа и выкорчевывания синонимов с веб-страницы
morph = pymorphy2.MorphAnalyzer()
re_pat = re.compile(u"a.*?SynonymsDictionary/%.*?>(.*?)<", re.M) # компилим регулярку, так быстрее и можно флаг прописать

# бегаем по всем словам
for my_str in result:
	_parse = morph.parse(my_str)
	try: # такая конструкция возьмет только первый результат списка и не выдаст ошибки, если вернулся не список
		parse=_parse[0]
	except:
		parse=_parse
	
	print u"\tСлово:" + my_str.lower()
	print u"\tЧасть речи: " + pos_parse(parse.tag.POS)
	print u"\tНормальная форма: " + parse.normal_form
	print u"\tПадеж: " + case_parse(str(parse.tag.case))
	print u"\tЧисло: " + number_parse(str(parse.tag.number))
	print u"\tТеги: " + str(parse.tag)

	if 1-sin_not_mask[parse.tag.POS]: # если подходит по маске - ищем синонимы
		r = requests.get(u"http://jeck.ru/tools/SynonymsDictionary/" + parse.normal_form)
		sin_list = re_pat.findall(r.text.encode("iso-8859-1"))

		# из списка в одну строку
		_str = ""
		for i in sin_list: _str = _str + i + ", "

		print u"\n\tСинонимы: " + _str.decode("utf-8").lower()
	print "====="
