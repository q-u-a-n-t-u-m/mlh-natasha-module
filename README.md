# MLH - Natasha Module #

MLH (My_Little_Homeserver) - small project that provides smart ans easy-to-control server system for personal purposes. This module is an control center of MLH with human-like behavior.

### What Natasha can? ###

* Give you some information through VK.com, e-mails, ICQ and SSH
* Take control of MLH's sub-systems like HTTP, FTP and Samba servers and many others
* Speak with you as (almost) real human
* Many other things...

### What already done? ###

* Morphological analysis of the words
* Synonym search

### How can I install it? ###
Sorry, but MLH project is only for Quantum Team's purposes, so there are no install tips for it. If you want it very much, contact with us.

### Contributors ###

Sky-WaLkeR, BlackLotos

### Contacts ###

shadowalker.main@gmail.com, ICQ:72471817